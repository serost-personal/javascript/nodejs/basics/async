#!/usr/bin/env node

// tested on node 21.6.2
import { setTimeout } from "node:timers/promises";

const getsomeval = async () => {
  // effectively suspends current coroutine, because it is waiting while setTimeout
  // promise is resolved (has returned and possible with some value)
  await setTimeout(5000, "go on now...");
  console.log("returning value"); // 3
  return "somevalue";
};

const main = async () => {
  await setTimeout(5000, "go on now..."); // same, suspends current coroutine for 5 sec
  // Since getsomeval is an asynchronous function, it returns a Promise immediately.
  const v = getsomeval(); // adds getsomeval to schedule
  console.log(`"v" is ${v}`); // 2, v is Promise<string>

  /* on await encounter, event loop checks if coroutine it is asking for finished,
   * and if not - suspends current coroutine, and picks another from queue
   */
  const waited_value = await v;
  console.log(waited_value); // 4
};

/* Main returns Promise<undefined> right away, and note, that it is never awaited.
 * main() is scheduled because it is async call, and will be resumed (started) when
 * global execution context finishes.
 */
main(); // adds main to schedule queue

console.log("Global Execution Context finished!"); // 1
