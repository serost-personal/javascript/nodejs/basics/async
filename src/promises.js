#!/usr/bin/env node

/*
 * async/await is simply a sugar for promises
 */

import { setTimeout } from "node:timers/promises";

const getsomeval = () => {
  return new Promise((resolve, reject) => {
    resolve(5);
  });
};

const getsomevalAsync = async () => {
  return 55;
};

const waitAndReturn = async () => {
  await setTimeout(5000, () => {});
  return "something";
};

const main = async () => {
  const cor = getsomeval();
  const val = await cor;
  console.log(val);

  // same thing
  const cor2 = getsomevalAsync();
  const val2 = await cor2;
  console.log(val2);

  // cor2 is already done, since awaited before
  cor2.then((val) => {
    console.log(`val from .then(): ${val}`);
  });

  try {
    /* if you not await promiseError(), it will throw error not within try block,
     * therefore giving you a runtime error.
     */
    await promiseError();
  } catch (e) {
    console.log(`Error catched! ${e}`);
  }
};

const promiseError = () => {
  return new Promise((resolve, reject) => {
    // pushes what to do when resolved to event loop
    setTimeout(3000, "wait...").then(() => {
      /* Put reject("No!") here to reject after 3 seconds
       * Then when 3 seconds will pass, this callback will
       * call reject function associated with specific Promise
       */
    });
    reject("No!"); // instantly rejects
  });
};

const asyncError = async () => {
  await setTimeout(3000, "wait..."); // suspends coroutine for 3 seconds
  throw new Error("No!"); // rejects after 3 seconds!
};

main();
console.log("Main is scheduled, but not running yet...");
