#!/usr/bin/env node

/* setTimeout coroutine adds timed event to the event loop and
 * yields out while waiting, and gives thread to some other functions.
 * i.e event loop saves its stackframe, restores stackframe and context
 * of some other coroutine (queued one), and runs it.
 */

const v = setTimeout(
  (someNumber, someObject) => {
    console.log(
      `I ran after 1 second with values: ${someNumber}, ${someObject.another}`
    );
  },
  1000,
  5,
  { another: "value" }
);

console.log("While setTimeout is still waiting, lets do more useful stuff...");
/*
 * https://stackoverflow.com/questions/10068981/what-does-settimeout-return
 * sometimes it is object, sometimes id... werid shit...
 * anyway, if you want to await setTimeout or something, you should use
 * import { setTimeout } from "node:timers/promises";
 */
console.log(`setTimeout returns ${v} of type ${typeof v}`);
